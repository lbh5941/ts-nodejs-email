/**
 * Created by ibyeonghun on 2017. 5. 5..
 */
import {loadTemplate, sendMail} from './util/EmailService'

// 여러 사용자
let users = [
  {
    name: 'lbh123',
    email: 'lbh5941@gmail.com',
  },
  {
    name: 'lbh345',
    email: 'lbh5941@gmail.com',
  },
  {
    name: 'lbh678',
    email: 'lbh5941@gmail.com',
  },
]

// 템플릿 설정 (marketing, notification, transactional)
loadTemplate('notification', users)
  .then(sendMail)
  .catch(err => console.error(err))



