/**
 * Created by ibyeonghun on 2017. 5. 6..
 */
import * as nodeMailer from 'nodemailer'
import * as smtpPool from 'nodemailer-smtp-pool'
import * as xoauth2 from 'xoauth2'
import * as path from 'path'
import * as emailTemplate from 'email-templates'

const rootPath = process.cwd().replace('/dist', '')
const authConfigPath: string = path.join(rootPath, '/config/auth-config.json')
const templateDir: string = path.join(rootPath, '/templates/')

const config = require(authConfigPath)

const xOAuth2gen: xoauth2 = xoauth2.createXOAuth2Generator({
  type: config.oauth.type,
  user: config.oauth.username,
  clientId: config.oauth.clientId,
  clientSecret: config.oauth.clientSecret,
  refreshToken: config.oauth.refreshToken,
})

xOAuth2gen.getToken((err, token, accessToken) => {
  if (err) {
    return console.error(err)
  }

  xOAuth2gen.accessToken = accessToken
})

const transporter = nodeMailer.createTransport(smtpPool({
  service: config.oauth.service,
  auth: {
    xoauth2: xOAuth2gen,
  },
}))

const EmailTemplate = emailTemplate.EmailTemplate

export function loadTemplate(templateName, contexts) {
  const template = new EmailTemplate(path.join(templateDir, templateName))

  return Promise.all(contexts.map((context) => {

    return new Promise((resolve, reject) => {
      template.render(context, (err, result) => {
        if (err) {
          reject(err)
        } else {
          resolve({email: result, context})
        }
      })
    })
  }))
}

export function sendMail(results) {
  return Promise.all(results.map((result) => {
    return new Promise((resolve, reject) => {
      const mailOption = {
        from: '관리자 <lbh5941@gmail.com>',
        to: result.context.email,
        subject: result.email.subject,
        html: result.email.html,
      }

      transporter.sendMail(mailOption, (err, res) => {
        if (err) {
          reject(err)
        } else {
          console.log('succeed... => ' + res.response)
          resolve()
        }

        transporter.close()
      })
    })
  }))
}

// export class EmailConfig {
//   private xoAuth2gen: xoauth2
//   private transporter: nodeMailer.Transporter | null = null
//   private EmailTemplate = emailTemplate.EmailTemplate
//
//   constructor() {
//     this.configXoAuth2gen()
//     this.setAccessToken()
//     this.createTransport()
//   }
//
//   private configXoAuth2gen(): void {
//     this.xoAuth2gen = xoauth2.createXOAuth2Generator({
//       type: config.oauth.type,
//       user: config.oauth.username,
//       clientId: config.oauth.clientId,
//       clientSecret: config.oauth.clientSecret,
//       refreshToken: config.oauth.refreshToken,
//     })
//   }
//
//   private setAccessToken(): void {
//     this.xoAuth2gen.getToken((err, token, accessToken) => {
//       if (err) {
//         return console.error(err)
//       }
//
//       this.xoAuth2gen.accessToken = accessToken
//     })
//   }
//
//   private createTransport(): void {
//     this.transporter = nodeMailer.createTransport(smtpPool({
//       service: config.oauth.service,
//       auth: {
//         xoauth2: this.xoAuth2gen,
//       },
//     }))
//   }
//
//   public loadTemplate(templateName: string, contexts: {name: string, email: string}[]): Promise<Object> {
//     const template: emailTemplate.EmailTemplate = new this.EmailTemplate(path.join(templateDir, templateName))
//     return Promise.all(contexts.map((context) => {
//       return new Promise((resolve, reject) => {
//         template.render(context, (err, result) => {
//           if (err) {
//             reject(err)
//           } else {
//             resolve({email: result, context})
//           }
//         })
//       })
//     }))
//   }
//
//   public sendMail(results) {
//     return Promise.all(results.map((result) => {
//       return new Promise((resolve, reject) => {
//         const mailOption = {
//           from: '관리자 <lbh5941@gmail.com>',
//           to: result.context.email,
//           subject: result.email.subject,
//           html: result.email.html,
//         }
//
//
//         this.transporter.sendMail(mailOption, (err, res) => {
//           if (err) {
//             reject(err)
//           } else {
//             console.log('succeed... => ' + res.response)
//             resolve()
//           }
//           this.transporter.close()
//         })
//       })
//     }))
//   }
//
//   public sendEmailTemplate(templateName: string, contexts: {name: string, email: string}[]): void {
//     this.loadTemplate(templateName, contexts)
//         .then(this.sendMail)
//         .catch(err => console.error(err))
//   }
// }