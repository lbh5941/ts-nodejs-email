## typescript/nodemailer project

[![Version](https://img.shields.io/badge/version-0.0.1-blue.svg)]()
[![node](https://img.shields.io/badge/nodejs-7.9.0-orange.svg)]()
[![typescript](https://img.shields.io/badge/typescript-2.3.2-orange.svg)]()
[![ecmascript](https://img.shields.io/badge/ecmascript-6-orange.svg)]()
[![License](https://img.shields.io/badge/license-private-lightgrey.svg)]()  


**Table of Contents**

- [What is the project](#what-is-the-project)
- [Status](#status)
- [Installation Guide](#installation-guide)

<br/>

### What is the Project
nodemailer 를 활용한 이메일 전송 프로젝트 입니다.

<br/>

### Status

Version 0.0.1

<br/>

### Installation Guide

이 프로젝트의 결과물을 설치하기 위해서는 다음 절차에 따라 설치하실 수 있습니다.

<br/>

#### How to download project source code

소스코드 관리 시스템에서 다음 명령으로 프로젝트를 내려 받습니다.

```bash
$ git clone https://github.com/byeong-hoon/ts-nodejs-send-email.git
```

<br/>

#### How to setting project structure


```
$ mkdir config
$ cd config
$ touch auth-config.json
```

```
# auth-config.json 에 작성

{
  "oauth": {
    "service": "Gmail",
    "type": "OAuth2",
    "username": "", 
    "clientId": "",
    "clientSecret": "",
    "refreshToken": ""
  }
}
```

```
$ npm install
$ tsc --init
$ tslint --init
$ tsc
```

<br/>

#### How to Run

프로젝트의 실행 방법은 다음과 같습니다.  

```bash
$ npm start
```

<br/>


### Tech. Stack

- NodeJS 7.9.0+
- EcmaScript 6+
- TypeScript 2.3.2+

<br/>

### License

프로젝트에 첨부되어 있는 [LICENSE](https://github.com/byeong-hoon/ts-nodejs-send-email/LICENSE)를 읽어보십시오.


<br />
**Byeong-Hoon**
